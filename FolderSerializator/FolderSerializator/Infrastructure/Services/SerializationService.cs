﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FolderSerializator.Model;
using MessageBox = System.Windows.MessageBox;

namespace FolderSerializator.Infrastructure.Services
{
    public static class SerializationService
    {
        public static string SerializeFolder()
        {
            try
            {
                string outputFile = @"serialized.bin";
                using (var stream = File.Open(outputFile, FileMode.OpenOrCreate))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    string path = GetFolderPath();
                    Folder folder = new Folder()
                    {
                        Folders = Directory.GetDirectories(path, "*", SearchOption.AllDirectories)
                                .Select(Path.GetFullPath).ToList(),
                        Files = Directory.GetFiles(path, "*", SearchOption.AllDirectories)
                                .Select(GetFileInfo).ToList()
                    };
                    formatter.Serialize(stream, folder);
                }
                return $"Serialization completed (File is at {Path.GetFullPath(outputFile)} )!";
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }

        private static FileInfo GetFileInfo(string path)
        {
            return new FileInfo(Path.GetFullPath(path));
        }

        public static string DeserializeFolder()
        {
            try
            {
                MessageBox.Show("Choose file to deserialize!");
                using (Stream stream = File.Open(GetFilePath(), FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    Folder folder = formatter.Deserialize(stream) as Folder;
                    if (folder == null)
                        return "Can not be deserialized";
                    MessageBox.Show("Choose folder, where to deserialize!");
                    folder.Unpack(GetFolderPath());
                }
                return $"Deserialization completed !";
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }

        private static string GetFilePath()
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Filter = "(*.bin)|*.bin"
            };
            dialog.ShowDialog();
            return dialog.FileName;
        }

        private static string GetFolderPath()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.ShowDialog();
                return dialog.SelectedPath;
            }
        }

    }
}
