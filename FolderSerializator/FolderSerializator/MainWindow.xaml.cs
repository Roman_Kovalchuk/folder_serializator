﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FolderSerializator.Infrastructure.Services;
using Microsoft.Win32;
using MessageBox = System.Windows.MessageBox;

namespace FolderSerializator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SerializeFolder(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(SerializationService.SerializeFolder());
        }

        private void DeserializeFolder(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(SerializationService.DeserializeFolder());
        }
    }
}
