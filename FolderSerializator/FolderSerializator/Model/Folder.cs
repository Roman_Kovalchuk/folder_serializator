﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderSerializator.Model
{
    [Serializable]
    class Folder
    {
        private List<string> _folders;

        private List<FileInfo> _files;

        public List<FileInfo> Files
        {
            get { return _files; }
            set { _files = value; }
        }

        public List<string> Folders
        {
            get { return _folders; }
            set { _folders = value; }
        }

        public void Unpack(string path)
        {
            string oldPath = Path.GetDirectoryName(_folders.FirstOrDefault());
            if (oldPath == null)
            {
                string filePath = Path.GetFullPath(_files.FirstOrDefault().FullName);
                oldPath = filePath.Substring(0, filePath.LastIndexOf('\\'));
            }
            if (_folders.Count > 0)
                _folders.ForEach(x => Directory.CreateDirectory(x.Replace(oldPath, path)));
            if (_files.Count > 0)
                _files.ForEach(x => CopyFile(x, oldPath, path));
        }

        private void CopyFile(FileInfo file, string oldPath, string path)
        {
            try
            {
                File.Copy(file.FullName, file.FullName.Replace(oldPath, path));
            }
            catch
            {
                File.Create(file.FullName.Replace(oldPath, path));
            }
        }
    }
}
